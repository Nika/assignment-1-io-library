%define SYS_WRITE 1
%define SYS_READ 0
%define SYS_EXIT 60
%define STDOUT 1
%define STDIN 0
%define SPACE 0x20
%define TAB 0x9
%define ENTER 0xA

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.count:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .count
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYS_WRITE 
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ENTER
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевеси ти цифры в их ASCII коды.
print_uint:
    xor r10, r10
    xor rax, rax
    dec rsp
    mov [rsp], al
    mov rax, rdi
    mov r8, 10
.loop:
    inc r10
    xor rdx, rdx
    div r8
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    test rax, rax
    jnz .loop
    mov rdi, rsp
    push r10
    call print_string
    pop r10
    add rsp, r10
    inc rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print
    mov r9, rdi
    mov rdi, '-'
    call print_char
    mov rdi, r9
    neg rdi
.print:    
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r9, r9
    xor r10, r10
    xor rdx, rdx
.loop:
    mov r9b, [rdi+rdx]
    mov r10b, [rsi+rdx]
    cmp r9b, r10b
    jne .neq
    cmp r9b, 0
    je .eq
    inc rdx
    jmp .loop
.eq:
    mov rax, 1
    ret
.neq:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYS_READ
    mov rdi, STDIN
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor r9, r9
.start_loop:
    push r9
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop r9
    cmp rax, TAB
    je .start_loop
    cmp rax, ENTER
    je .start_loop
    cmp rax, SPACE
    je .start_loop
    cmp rax, 0
    je .yes
.loop:
    mov [rdi+r9], rax
    inc r9
    cmp r9, rsi
    ja .no
    push r9
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    pop r9
    cmp rax, 0
    je .yes
    cmp rax, TAB
    je .yes
    cmp rax, ENTER
    je .yes
    cmp rax, SPACE
    je .yes
    jmp .loop
.yes:
    xor rax, rax
    mov [rdi+r9], rax
    mov rax, rdi
    mov rdx, r9
    ret
.no:
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r10, r10
    xor rax, rax
    xor r9, r9
    mov r8, 10
.loop:
    mov r9b, [rdi+r10]
    cmp r9, '0'
    jb .stop
    cmp r9, '9'
    ja .stop
    mul r8
    sub r9, '0'
    add rax, r9
    inc r10
    jmp .loop
.stop:
    mov rdx, r10
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r9, r9
    mov r9b, [rdi]
    xor r8, r8
    cmp r9, '-'
    je .sign
    cmp r9, '+'
    je .sign
.read:
    push r9
    push r8
    call parse_uint
    pop r8
    pop r9
    test rdx, rdx
    jz .end
    add rdx, r8
    cmp r9, '-'
    jne .end
    neg rax
    jmp .end
.sign:
    inc rdi
    inc r8
    jmp .read
.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    xor r8, r8
.loop:    
    mov r9,[rdi+r8]
    mov [rsi+r8], r9
    cmp r8, rax
    je .compare
    inc r8
    jmp .loop
.compare: 
    cmp rax, rdx
    ja .no
    ret
.no: 
    mov rax, 0 
    ret
